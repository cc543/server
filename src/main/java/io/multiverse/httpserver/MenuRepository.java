package io.multiverse.httpserver;

import org.springframework.data.jpa.repository.JpaRepository;

interface MenuRepository extends JpaRepository<Menu, Integer> {
    
}
