let data = []
const state = {
    menuOpen: null
}

fetch('http://localhost/restaurants')
    .then(response => response.json())
    .then(_data =>
        {data = _data
        render()
    })
    .catch(err=> console.error(err))

    function getAddFormHtml(){
        return `
        <article>
            <form onsubmit="event.preventDefault();addCafe(this);">
            <div style="background-image: url('https://media.istockphoto.com/vectors/black-plus-sign-positive-symbol-vector-id688550958?k=20&m=688550958&s=612x612&w=0&h=wvzUqT3u3feYygOXg3GB9pYBbqIsyu_xpvfTX-6HOd0=');"></div>
                <input name="name" required placeholder="Enter Restaurant Name" />
                <input name="imageURL" type="url" required placeholder="Enter Image URL" />
                <button>Add Cafe</button>
            </form>
        </article>
        `
    }

    function render(){
        let content = data.map((cafeData, i) => {
            return `
                <article class=rest-card>
                    <div style="background-image: url('${cafeData.imageURL}');"></div>
                    <footer>
                        <h2 id="${i}-header">${cafeData.name}</h2>
                        <button onclick="displayMenus(${i})">Menus</button>
                    <footer>
                </article>
            `
        }).join("")

        content += getAddFormHtml()

        const appEl = document.getElementById('app')
        appEl.innerHTML = content

        if (state.menuOpen){
            const modalContent =
                   `<section class="modal-bg">
                    <article>
                        ${state.menuOpen.map(menu =>{
                            return `<h3>${menu.title}</h3>`
                        }).join("")}
                        <button onclick="closeModal()">close</button>
                    </article>
                    </section> `               
            const modalEl = document.getElementById('modal')
            modalEl.innerHTML = modalContent
        } else {
            const modalEl = document.getElementById('modal')
            modalEl.innerHTML = ""
        }
    }

    function displayMenus(index){
        state.menuOpen = data[index].menus
        render()
    }

    function addCafe(HTMLform){
        console.log(HTMLform)
        const form = new FormData(HTMLform)
        const name = form.get('name')
        const imageURL = form.get('imageURL')
        fetch('/restaurants', {
            method: 'POST',
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({name, imageURL})
        })
        .then(res=>res.json())
        .then(cafe=>{
            data.push(cafe)
            render()
        })
        .catch(console.error)
    }


    function closeModal(){
        state.menuOpen = null
        render()
    }
    